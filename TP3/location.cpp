#include "location.h"

Location::Location(){
}

Location::Location(int idClient, int idProduit){
    _idClient = idClient;
    _idProduit = idProduit;
}

int Location::getIdClient() const{
    return _idClient;
}

int Location::getIdProduit() const{
    return _idProduit;
}

void Location::afficherLocation() const{
    std::cout << "Location (" << _idClient << " ," << _idProduit << ")" << std::endl;
}
