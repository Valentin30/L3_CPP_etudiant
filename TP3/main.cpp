#include "location.h"
#include "client.h"
#include "produit.h"
#include "magasin.h"

int main(){

    Magasin m;

    m.ajouterClient("Jean");
    m.ajouterClient("Michel");
    m.ajouterClient("Jeansdfsfgsfd");
    m.ajouterClient("Michel3546544");

    m.afficherClients();

    m.ajouterProduit("produit 1");
    m.ajouterProduit("produit 2");
    m.ajouterProduit("produit 3");
    m.ajouterProduit("produit 4");

    m.afficherProduits();

    m.ajouterLocation(1, 3);
    m.ajouterLocation(1, 3);
    m.ajouterLocation(1, 2);
    m.ajouterLocation(7,49);

    m.supprimerLocation(1, 3);
    m.afficherLocation();
    return 0;
}
