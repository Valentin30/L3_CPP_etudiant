#ifndef MAGASIN_H
#define MAGASIN_H
#include <vector>
#include<string>

#include "client.h"
#include "produit.h"
#include "location.h"

class Magasin{
private:
    std::vector<Client> _clients;
    std::vector<Produit> _produits;
    std::vector<Location> _locations;
    int _idCourantClient, _idCourantProduit;

public:
    Magasin();

    //Gestion clients
    int nbClients() const;
    void ajouterClient(const std::string & nom);
    void afficherClients() const;
    void supprimerClient(int idClient);

    //Gestion produits
    int nbProduits() const;
    void ajouterProduit(const std::string & nom);
    void afficherProduits() const;
    void supprimerProduits(int idProduit);

    //Fin d’implementation de la classe Magasin
    int nbLocations() const;
    void ajouterLocation(int idClient, int idProduit);
    void afficherLocation() const;
    void supprimerLocation(int idClient, int idProduit);
    bool trouverClientDansLocation(int idClient) const;
    std::vector<int> calculerClientsLibres() const;
    bool trouverProduitDansLocation(int idProduit) const;
    std::vector<int> calculerProduitsLibres() const;
};

#endif // MAGASIN_H
