#include "produit.h"
#include "client.h"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupProduit) { };
TEST_GROUP(GroupClient) { };

TEST(GroupProduit, Produit_test1){
    Produit pdt(1, "Un produit");
    CHECK_EQUAL(pdt.getId(), 1);
    CHECK_EQUAL(pdt.getDescription(), "Un produit");
}

TEST(GroupClient, Client_test1){
    Client cl(42, "toto");
    CHECK_EQUAL(cl.getId(), 42);
    CHECK_EQUAL(cl.getNom(), "toto");
}
