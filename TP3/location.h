#ifndef LOCATION_H
#define LOCATION_H

#include <iostream>

struct Location{
    int _idClient, _idProduit;

    Location();
    Location(int idClient, int idProduit);
    int getIdClient() const;
    int getIdProduit() const;
    void afficherLocation() const;
};

#endif // LOCATION_H
