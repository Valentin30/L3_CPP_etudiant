#ifndef PRODUIT_H
#define PRODUIT_H

#include <iostream>
using namespace std;

class Produit{
private:
    int _id;
    std::string _description;

public:
    Produit();
    Produit(int id, const std::string & description);
    int getId() const;
    const std::string & getDescription() const;
    void afficherProduit() const;
};

#endif // PRODUIT_H
