#ifndef CLIENT_H
#define CLIENT_H

#include <string>
#include <iostream>

class Client{
private:
    int _id;
    std::string _nom;

public:
    Client();
    Client(int id,const std::string & nom);
    int getId() const;
    const std::string & getNom() const;
    void afficherClient() const;
};

#endif // CLIENT_H
