#include "magasin.h"
#include <iostream>
using namespace std;

Magasin::Magasin(){
    _idCourantClient = 0;
    _idCourantProduit = 0;
}

int Magasin::nbClients() const{
    return _clients.size();
}

void Magasin::ajouterClient(const std::string & nom){
    _clients.push_back(Client(_idCourantClient, nom));
    _idCourantClient ++;
}

void Magasin::afficherClients() const{
    std::cout << std::endl << "Clients du magasin : " << std::endl;
    for(unsigned int i = 0; i < _clients.size(); i++){
        std::cout << "      -";
        _clients.at(i).afficherClient();
    }
}

void Magasin::supprimerClient(int idClient){
    try {
        if (idClient == _clients.at(idClient).getId()) {
            swap(_clients.at(idClient),_clients.back());
            _clients.pop_back();
        }
    }catch ( std::out_of_range e){
            std::cout << std::endl << "Erreur du client " <<
                         idClient << " : ce client n’existe pas" << std::endl;
       }
}

int Magasin::nbProduits() const{
    return _produits.size();
}

void Magasin::ajouterProduit(const std::string & nom){
    _produits.push_back(Produit(_idCourantProduit, nom));
    _idCourantProduit++;
}

void Magasin::afficherProduits() const{
    std::cout << std::endl << "Produits du magasin : " << std::endl;
    for(unsigned int i = 0; i < _produits.size(); i++){
        std::cout << "      -";
        _produits.at(i).afficherProduit();
    }
}

void Magasin::supprimerProduits(int idProduit){
    try {
        if (idProduit == _produits.at(idProduit).getId()) {
            swap(_produits.at(idProduit),_produits.back());
            _produits.pop_back();
        }
    }catch ( std::out_of_range e){
            std::cout << std::endl << "Erreur du produit" <<
                         idProduit << " : ce produit n’existe pas" << std::endl;
       }
}

int Magasin::nbLocations() const{
    return _locations.size();
}

void Magasin::ajouterLocation(int idClient, int idProduit){
    try{
        if (idClient == _clients.at(idClient).getId()) {
            try{
                if (idProduit == _produits.at(idProduit).getId()) {
                    _locations.push_back(Location(idClient, idProduit));
                }
            }catch(std::out_of_range e){
                std::cout << std::endl << "Erreur : Produit n°" << idProduit << " inexistant !" << std::endl;
            }
        }
    }catch ( std::out_of_range e){
        std::cout << std::endl << "Erreur : Client n°" << idClient << " inexistant !" << std::endl;
    }
}

void Magasin::afficherLocation() const{
    std::cout << std::endl << "Locations du magasin : " << std::endl;
    for(unsigned int i = 0; i < _locations.size(); i++){
        std::cout << "      -";
        _locations.at(i).afficherLocation();
    }
}

void Magasin::supprimerLocation(int idClient, int idProduit){
}

bool Magasin::trouverClientDansLocation(int idClient) const{
}

std::vector<int> Magasin::calculerClientsLibres() const{
}

bool Magasin::trouverProduitDansLocation(int idProduit) const{
}

std::vector<int> Magasin::calculerProduitsLibres() const{
}
