#include <iostream>
#include "fibonacci.h"

int fibonacciRecursif(int n){
    if (n<=1){
        return n;
    }else{
        return fibonacciRecursif(n-1) + fibonacciRecursif(n-2);
    }
}

int fibonacciIteratif(int n){
  int i = 0;
  int j = 1;
  int temp;

  for(int k=0; k<n; k++){
      temp = i + j;
      i = j;
      j = temp;
  }
  return i;
}
