#ifndef VECTEUR3_H
#define VECTEUR3_H

struct vecteur3{
    float _x, _y, _z;
    void afficher();
};

void afficher(const vecteur3 & v);
void norme(const vecteur3 & v);
void produitScal(vecteur3 v1, vecteur3 v2);
void addition(vecteur3 v1, vecteur3 v2);

#endif // VECTEUR3_H

