#include <iostream>
#include <math.h>
#include "vecteur3.h"

using namespace std;

void vecteur3::afficher(){
    cout << "Vecteur (" << _x << "," << _y << "," << _z << ")" << endl;
}

void afficher(const vecteur3 & v){
    cout << "Vecteur (" << v._x << "," << v._y << "," << v._z << ")" << endl;
}

void norme(const vecteur3 & v){
    cout << sqrt(pow(v._x,2) + pow(v._y,2) + pow(v._z,2)) << endl;
}

void produitScal(vecteur3 v1, vecteur3 v2){
	double resultat;
    resultat = (v1._x*v2._x) + (v1._y*v2._y) + (v1._z*v2._z);
	cout << resultat << endl;
}

void addition(vecteur3 v1, vecteur3 v2){
    double resultat;
    resultat = (v1._x+v2._x) + (v1._y+v2._y) + (v1._z+v2._z);
    cout << resultat << endl;
}
