#include "fibonacci.h"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibonacciRecursif) { };
TEST_GROUP(GroupFibonacciIteratif) { };

TEST(GroupFibonacciRecursif,test_fibonacci_1){
    int listResult[]={0,1,1,2,3};
    for(int i=0; i<5; i++){
        CHECK_EQUAL(listResult[i],fibonacciRecursif(i));
    }
}

TEST(GroupFibonacciIteratif,test_fibonacci_2){
    int listResult[]={0,1,1,2,3};
    for(int i=0; i<5; i++){
        CHECK_EQUAL(listResult[i],fibonacciIteratif(i));
    }
}
