#include <iostream>
#include "fibonacci.h"
#include "vecteur3.h"

using namespace std;

int main(){
    int i =7;
    cout << "fibonacciIteratif : " << fibonacciIteratif(i) << endl;
    cout << "fibonacciRecursif : " << fibonacciRecursif(i) << endl;

    vecteur3 vector;
    vector._x = 2.0;
    vector._y = 3.0;
    vector._z = 6.0;

    vector.afficher();
    afficher(vector);
    norme(vector);

    return 0;
}
