#include "bibliotheque.h"
#include "livre.h"
#include <iostream>

int main() {
    Bibliotheque b;
    Livre l1("ABC","BCA", 123);
    Livre l2("HFG","BCA", 7653);
    b.push_back(l2);
    b.push_back(l1);

    b.afficher();
    b.trierParAuteurEtTitre();
    b.afficher();
    return 0;
}

