#include "bibliotheque.h"

Bibliotheque::Bibliotheque(){
}

void Bibliotheque::afficher() const{
    std::cout << "[Bibliotheque]" << std::endl;
    for (unsigned int i = 0; i < this->size(); ++i) {
        std::cout << (*this)[i].getTitre()
                  << " ," << (*this)[i].getAuteur()
                  << " ," << (*this)[i].getAnnee() << std::endl;
    }
}

void Bibliotheque::trierParAuteurEtTitre(){
    std::sort(this->begin(), this->end());
    std::cout << "Livres triés par auteurs et titres" << std::endl;
}

void Bibliotheque::trierParAnnee(){
    std::sort(this->begin(), this->end(),[](Livre& livre1, Livre& livre2){
        return livre1.getAnnee() < livre2.getAnnee();
    });
    std::cout << "Livres triés par années" << std::endl;
}

void Bibliotheque::lireFichier(const std::string & nomFichier){
    std::ifstream ifs(nomFichier);
    if(!ifs){
        std::cerr << "Le fichier [" << nomFichier << "] n'existe pas" << std::endl;
        return 1;
    }
    std::string line;
    while(std::getline(ifs,line))
        std::cout << line << std::endl;
    ifs.close();
    return 0;
}


void Bibliotheque::ecrireFichier(const std::string & nomFichier) const{

}
