#include "livre.h"

Livre::Livre(){
}

Livre::Livre(const std::string & titre, const std::string & auteur, int annee){
    if (titre.find(";") != std::string::npos) {
        throw std::string("erreur : titre non valide (';' non autorisé)");
    }
    else if (titre.find("\n") != std::string::npos) {
        throw std::string("erreur : titre non valide ('\n' non autorisé)");
    }
    else if (auteur.find(";") != std::string::npos) {
        throw std::string("erreur : auteur non valide (';' non autorisé)");
    }
    else if (auteur.find("\n") != std::string::npos) {
        throw std::string("erreur : auteur non valide ('\n' non autorisé)");
    }

    _titre = titre;
    _auteur = auteur;
    _annee = annee;
}

const std::string & Livre::getTitre() const{
    return _titre;
}

const std::string & Livre::getAuteur() const{
    return _auteur;
}

int Livre::getAnnee() const{
    return _annee;
}


bool Livre::operator<(const Livre & livre) const{
    if (_auteur != livre._auteur) {
        return _auteur<livre._auteur;
    }else{
        return _titre<livre._titre;
    }
}

bool Livre::operator==(const Livre & livre) const{
    if (_auteur == livre._auteur && _titre == livre._titre && _annee == livre._annee) {
        return _auteur == livre._auteur && _titre == livre._titre && _annee == livre._annee;
    }
    return false;
}

std::ostream & operator<<(std::ostream & os, const Livre & livre){
    os << livre.getTitre() << ";" << livre.getAuteur() << ";" << livre.getAnnee();
    return os;
}

std::istream & operator>>(std::istream & is, Livre & livre){
    std::string line;
    while (getline(is, line)) {
        std::istringstream iss(line);

        std::string titre, auteur, annee;

        getline(iss, titre, ';');
        getline(iss, auteur, ';');
        getline(iss, annee, ' ');

        livre._titre = titre;
        livre._auteur = auteur;
        livre._annee = stoi(annee);
    }
    return is;
}

