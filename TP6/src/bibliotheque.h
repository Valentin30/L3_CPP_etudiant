#ifndef BIBLIOTHEQUE_H
#define BIBLIOTHEQUE_H

#include <istream>
#include "livre.h"
#include <vector>
#include <algorithm>

class Bibliotheque : public std::vector<Livre>{
public:
    Bibliotheque();
    void afficher() const;
    void trierParAuteurEtTitre();
    void trierParAnnee();
    void lireFichier(std::string & nomFichier const);
    void ecrireFichier(std::string & nomFichier const);
};

#endif // BIBLIOTHEQUE_H
