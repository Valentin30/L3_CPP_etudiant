#ifndef LIVRE_H
#define LIVRE_H

#include <iostream>
#include <ostream>
#include <sstream>

class Livre{
private:
//    std::string _titre;
//    std::string _auteur;
//    int _annee;

public:
    std::string _titre;
    std::string _auteur;
    int _annee;
    Livre();
    Livre(const std::string & titre, const std::string & auteur, int annee);
    const std::string & getTitre() const;
    const std::string & getAuteur() const;
    int getAnnee() const;
    bool operator<(const Livre & livre) const;
    bool operator==(const Livre & livre) const;
};

std::ostream & operator<<(std::ostream & os, const Livre & livre);
std::istream & operator>>(std::istream & is, Livre & Livre);

#endif // LIVRE_H
