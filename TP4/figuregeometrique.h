#ifndef FIGUREGEOMETRIQUE_H
#define FIGUREGEOMETRIQUE_H

#include "couleur.h"

class FigureGeometrique{
public:
    Couleur _couleur;

    FigureGeometrique();
    FigureGeometrique(const Couleur & couleur);
    const Couleur & getCouleur() const;
    virtual void afficher() const;

};

#endif // FIGUREGEOMETRIQUE_H
