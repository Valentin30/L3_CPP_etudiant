#include "polygoneregulier.h"
#include <iostream>
#include <math.h>

PolygoneRegulier::PolygoneRegulier(){
}

PolygoneRegulier::PolygoneRegulier(const Couleur & couleur, const Point & centre, int rayon, int nbCotes){
    _couleur = couleur;
    _nbPoints = nbCotes;
    _points = new Point[nbCotes];

    for (int i = 0; i < nbCotes; ++i) {
        _points[i]._x = (rayon * cos(i * M_PI / nbCotes) + centre._x);
        _points[i]._y = (rayon * sin(i * M_PI / nbCotes) + centre._y);
    }
}

PolygoneRegulier::~PolygoneRegulier(){
    delete[] _points;
}

void PolygoneRegulier::afficher() const{
    std::cout << "PolygoneRegulier " << _couleur._r << "_" << _couleur._g << "_" << _couleur._b << " ";
    for (int i = 0; i < _nbPoints; ++i) {
       std::cout << _points[i]._x << "_" << _points[i]._y << " ";
    }
    std::cout << std::endl;
}

int PolygoneRegulier::getNbPoints() const{
    return _nbPoints;
}

const Point & PolygoneRegulier::getPoint(int indice) const{
    return _points[indice];
}
