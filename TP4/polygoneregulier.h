#ifndef POLYGONEREGULIER_H
#define POLYGONEREGULIER_H

#include "figuregeometrique.h"
#include "point.h"

class PolygoneRegulier : public FigureGeometrique{

private:
    int _nbPoints;
    Point *_points;

public:
    PolygoneRegulier();
    PolygoneRegulier(const Couleur & couleur, const Point & centre, int rayon, int nbCotes);
    ~PolygoneRegulier();
    virtual void afficher() const;
    int getNbPoints() const;
    const Point & getPoint(int indice) const;
};

#endif // POLYGONEREGULIER_H
