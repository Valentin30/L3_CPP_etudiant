#include "figuregeometrique.h"
#include "polygoneregulier.h"
#include "ligne.h"

#include <iostream>
#include <vector>

int main() {
    Couleur c;
    c._r = 0.0;
    c._g = 1.0;
    c._b = 0.0;

    Point centre, p0, p1;
    centre._x = 100;
    centre._y = 200;
    p0._x = 0.0;
    p0._y = 0.0;
    p1._x = 200;
    p1._y = 100;

    std::vector<FigureGeometrique*> fg {new Ligne(c, p0, p1), new PolygoneRegulier(c, centre, 50, 5)};

    for (FigureGeometrique * ptrFigGeo : fg){
        ptrFigGeo->afficher();
    }

    for (FigureGeometrique * ptrFigGeo : fg){
        delete ptrFigGeo;
    }

    return 0;
}

