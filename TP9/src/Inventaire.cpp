#include "Inventaire.hpp"
#include <locale>
#include <algorithm>

void Inventaire::trier(){
    std::sort(_bouteilles.begin(), _bouteilles.end(), [](const Bouteille & a, const Bouteille & b)->bool{
        return a._nom < b._nom;
    });
}

std::ostream & operator<<(std::ostream & os, Inventaire & inv){
    /*for (Bouteille b : inv._bouteilles){
        os << b._nom << ";" << b._date << ";" << b._volume << "\n" ;
    }

    std::list<Bouteille>::iterator it;
    for (it = inv._bouteilles.begin(); it != inv._bouteilles.end(); ++it){
        os << it->_nom << ";" << it->_date << ";" << it->_volume << "\n" ;
    }*/
    std::for_each(inv._bouteilles.begin(), inv._bouteilles.end(), [&](Bouteille b){
        os << b;
    });

    return os;
}

std::istream & operator >>(std::istream & is, Inventaire & inv) {
    Bouteille b;
    while(is >> b) {
        inv._bouteilles.push_back(b);
    }
    return is;
}
