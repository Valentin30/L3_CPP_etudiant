#ifndef INVENTAIRE_HPP_
#define INVENTAIRE_HPP_

#include "Bouteille.hpp"

#include <iostream>
#include <vector>
#include <string>
#include <list>

// Modèle : inventaire de bouteilles.
struct Inventaire {
    std::vector<Bouteille> _bouteilles;

    void trier();
};

#endif

std::ostream & operator<<(std::ostream & os, Inventaire & inv);
std::istream& operator >>(std::istream& is, Inventaire & inv);
