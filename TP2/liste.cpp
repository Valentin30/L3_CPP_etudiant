#include "liste.hpp"
#include <iostream>

Liste::Liste(){
    _tete = nullptr;
}

Liste::~Liste(){
    Noeud *n = _tete;
    while(n != nullptr){
        Noeud *efface = n->_suivant;
        delete n;
        n = efface;
    }
}

void Liste::ajouterDevant(int valeur){
    Noeud *nvNoeud = new Noeud;
    nvNoeud->_valeur = valeur;
    nvNoeud->_suivant = _tete;
    _tete = nvNoeud;
}

int Liste::getTaille() const{
    int i(0);
    Noeud *n = _tete;
    while(n != nullptr){
        i++;
        n = n->_suivant;
    }
    return i;
}

int Liste::getElement(int indice) const{
    int i=1;
    Noeud *n = _tete;
    while(i != indice){
        n = n->_suivant;
        i++;
    }
    return n->_valeur;
}

void afficherListe(Liste *liste){
    Noeud *n = liste->_tete;
    for(int i = 0 ; i<liste->getTaille(); i++){
        std::cout << n->_valeur;
        n = n->_suivant;
        if(n != nullptr){
            std::cout << " -> ";
        }
    }
    std::cout << std::endl;
}
