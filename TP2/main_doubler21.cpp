#include "doubler.hpp"
#include "liste.hpp"
#include <iostream>

int main() {
    std::cout << doubler(21) << std::endl;
    std::cout << doubler(21.f) << std::endl;

    // Pointeurs
    int a(42);
    int *p(0);
    p=&a;
    *p=37;

    //Allocation dynamique
    int *t(0);
    t= new int[10];
    t[3] = 42;
    delete [] t;
    t= nullptr;

    std::cout << a << *p << std::endl; 

    //Listes
    Liste *liste = new Liste();
    liste->ajouterDevant(7);
    liste->ajouterDevant(3);
	
    afficherListe(liste);
    std::cout << "Taille -> " << liste->getTaille() << std::endl;
	
    std::cout << "Element n°2 -> " << liste->getElement(2) << std::endl;
	
    //Analyse des fuites mémoires

    Liste *liste2 = new Liste();
    liste2->ajouterDevant(13);
    liste2->ajouterDevant(37);

    for (int i = 1; i <= liste2->getTaille(); ++i) {
        std::cout << liste->getElement(i);
        if (i != liste2->getTaille()) {
            std::cout << " -> ";
        }else{
            std::cout << std::endl;
        }
    }

    delete liste;
    delete liste2;

    return 0;
}
