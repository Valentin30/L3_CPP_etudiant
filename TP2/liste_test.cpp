#include "liste.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupList) { };

TEST(GroupList, List_test1)  {
    Liste *liste = new Liste();
    liste->ajouterDevant(7);
    liste->ajouterDevant(3);

    CHECK_EQUAL(liste->getElement(1), 3);
    delete liste;
}

TEST(GroupList, List_test2)  {
    Liste *liste = new Liste();
    liste->ajouterDevant(7);
    liste->ajouterDevant(3);

    CHECK_EQUAL(liste->getTaille(), 2);
    delete liste;
}
