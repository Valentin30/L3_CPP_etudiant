#include <iostream>

#include "Liste.hpp"
#include "ListeGenerique.h"

int main() {
    std::cout << "Liste normale" << std::endl;

    Liste l1;
    l1.push_front(37);
    l1.push_front(13);
    std::cout << l1 << std::endl;


    std::cout << "Liste generique float" << std::endl;

    ListeGenerique<float> l2;
    l2.push_front(37.35);
    l2.push_front(13.12);
    std::cout << l2 << std::endl;

    return 0;
}

