#ifndef LISTEGENERIQUE_H
#define LISTEGENERIQUE_H

#include <cassert>
#include <ostream>

template <typename T>

// liste d'entiers avec itérateur
class ListeGenerique {
private:
    struct NoeudGenerique {
        T _valeur;
        NoeudGenerique * _ptrNoeudSuivant;
    };

protected:
    NoeudGenerique * _ptrTete;

public:
    class iteratorGenerique {
    protected :
        NoeudGenerique * _ptrNoeudCourant;

    public:

        iteratorGenerique(NoeudGenerique * ptrNoeudCourant){
            _ptrNoeudCourant = ptrNoeudCourant;
        }

        const iteratorGenerique & operator++() {
            _ptrNoeudCourant = _ptrNoeudCourant->_ptrNoeudSuivant;
            return *this;
        }

        T& operator*() const {
            return _ptrNoeudCourant->_valeur;
        }

        bool operator!=(const iteratorGenerique & it) const {
            if (it._ptrNoeudCourant == _ptrNoeudCourant) {
                return false;
            }
            return true;
        }

        friend ListeGenerique;
    };

public:
    ListeGenerique(){
        _ptrTete = nullptr;
    }

    ~ListeGenerique(){
        clear();
    }

    void push_front(T valeur) {
        NoeudGenerique* noeud = new NoeudGenerique;
        noeud->_valeur = valeur;
        noeud->_ptrNoeudSuivant = _ptrTete;
        _ptrTete = noeud;
    }

    T& front() const {
        return _ptrTete->_valeur;
    }

    void clear() {
        NoeudGenerique* noeud;
        while (_ptrTete != nullptr) {
            noeud =_ptrTete->_ptrNoeudSuivant;
            delete _ptrTete;
            _ptrTete = noeud;
        }
    }

    bool empty() const {
        NoeudGenerique* noeud = _ptrTete;
        if (noeud != nullptr) {
            return false;
        }
        return true;
    }

    iteratorGenerique begin() const {
        return iteratorGenerique(_ptrTete);
    }

    iteratorGenerique end() const {
        return iteratorGenerique(nullptr);
    }
};

template <typename T>

std::ostream& operator<<(std::ostream& os, const ListeGenerique<T> & liste) {
    std::locale vieuxLoc = std::locale::global(std::locale("fr_FR.UTF-8"));

    /*ListeGenerique::iteratorGenerique it = liste.begin();
    while (it != liste.end()) {
        os << *it << " ";
        ++it;
    }*/
    for (T& v : liste){
        os << v << " ";
    }

    std::locale::global(vieuxLoc);
    return os;
}

#endif // LISTEGENERIQUE_H
