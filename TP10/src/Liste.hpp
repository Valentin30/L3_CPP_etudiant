
#ifndef LISTE_HPP_
#define LISTE_HPP_

#include <cassert>
#include <ostream>

// liste d'entiers avec itérateur
class Liste {
private:
    struct Noeud {
        int _valeur;
        Noeud * _ptrNoeudSuivant;
    };

protected:
    Noeud * _ptrTete;

public:
    class iterator {
    protected :
        Noeud * _ptrNoeudCourant;

    public:

        iterator(Noeud * ptrNoeudCourant){
            _ptrNoeudCourant = ptrNoeudCourant;
        }

        const iterator & operator++() {
            _ptrNoeudCourant = _ptrNoeudCourant->_ptrNoeudSuivant;
            return *this;
        }

        int& operator*() const {
            return _ptrNoeudCourant->_valeur;
        }

        bool operator!=(const iterator & it) const {
            if (it._ptrNoeudCourant == _ptrNoeudCourant) {
                return false;
            }
            return true;
        }

        friend Liste;
    };

public:
    Liste(){
        _ptrTete = nullptr;
    }

    ~Liste(){
        clear();
    }

    void push_front(int valeur) {
        Noeud* noeud = new Noeud;
        noeud->_valeur = valeur;
        noeud->_ptrNoeudSuivant = _ptrTete;
        _ptrTete = noeud;
    }

    int& front() const {
        return _ptrTete->_valeur;
    }

    void clear() {
        Noeud* noeud;
        while (_ptrTete != nullptr) {
            noeud =_ptrTete->_ptrNoeudSuivant;
            delete _ptrTete;
            _ptrTete = noeud;
        }
    }

    bool empty() const {
        Noeud* noeud = _ptrTete;
        if (noeud != nullptr) {
            return false;
        }
        return true;
    }

    iterator begin() const {
        return iterator(_ptrTete);
    }

    iterator end() const {
        return iterator(nullptr);
    }
};

std::ostream& operator<<(std::ostream& os, const Liste& liste) {
    std::locale vieuxLoc = std::locale::global(std::locale("fr_FR.UTF-8"));

    Liste::iterator it = liste.begin();
    while (it != liste.end()) {
        os << *it << " ";
        ++it;
    }

    std::locale::global(vieuxLoc);
    return os;
}

#endif
