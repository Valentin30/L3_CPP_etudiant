#include "zonedessin.h"
#include <iostream>

ZoneDessin::ZoneDessin(){
    add_events(Gdk::BUTTON_PRESS_MASK);
    this->signal_button_press_event().connect(sigc::mem_fun(*this,&ZoneDessin::gererClic));
}

ZoneDessin::~ZoneDessin(){
    for (FigureGeometrique * ptrFigGeo : _figures){
        delete ptrFigGeo;
    }
}

bool ZoneDessin::on_expose_event(GdkEventExpose* event){
    Cairo::RefPtr<Cairo::Context> context = get_window()->create_cairo_context();
    for(std::vector<FigureGeometrique*>::const_iterator it=_figures.begin();it!=_figures.end();++it){
        (*it)->afficher(context);
    }
    return true;
}

bool ZoneDessin::gererClic(GdkEventButton* event){
    Couleur c(0.0, 0.1, 0.0);

    Point p(event->x,event->y);
    int nbCote = rand() % 15;
    int taille = rand() % 100 +1;

    if(nbCote > 50 || nbCote == 0){
        nbCote = 50;
    }
    if ( taille > 50 || taille == 0) {
        taille = 50;
    }

    _figures.push_back(new PolygoneRegulier(c,p,taille,nbCote));
    queue_draw();

    /*if( (event->type == GDK_BUTTON_PRESS) && (event->button == 1) ){
        _figures.push_back(new PolygoneRegulier(c, p, 50,5));
    }
    if( (event->type == GDK_BUTTON_PRESS) && (event->button == 3) ){
        _figures.pop_back();
    }*/
}
