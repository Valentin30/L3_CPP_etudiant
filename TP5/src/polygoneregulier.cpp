#include "polygoneregulier.h"
#include <iostream>
#include <math.h>

PolygoneRegulier::PolygoneRegulier(const Couleur& couleur,const Point& centre,int rayon,int nbCote)
    :FigureGeometrique(couleur),_nbPoints(nbCote){

    float delta = 2.f * 3.14 / nbCote;
    _points = new Point[nbCote];
    for (int i = 0; i < nbCote; i++) {
        _points[i] = Point(
        static_cast<int>(centre.getX() +  rayon * cos(delta * i)),
        static_cast<int>(centre.getY() + rayon * sin(delta * i)));
    }
}

void PolygoneRegulier::afficher(const Cairo::RefPtr<Cairo::Context> & context) const{
	for(int i=0;i<getNbPoints();i++){
        context->move_to(_points[i].getX(),_points[i].getY());
        
        if(i!=getNbPoints()-1){
            context->line_to(_points[i+1].getX(),_points[i+1].getY());
        }else{
            context->line_to(_points[0].getX(),_points[0].getY());
        }
        context->set_line_width(2.0);
		context->set_source_rgb(getCouleur().getR(),getCouleur().getG(),getCouleur().getB());
        context->stroke();
    }
}

int PolygoneRegulier::getNbPoints() const{
    return _nbPoints;
}

const Point PolygoneRegulier::getPoint(int indice) const{
    return _points[indice];
}
