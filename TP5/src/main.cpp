#include <gtkmm.h>
#include "viewerfigures.h"

int main(int argc, char* argv[]) {
    ViewerFigures vF(argc, argv);
    vF.run();
    return 0;
}
