#ifndef POINT_H
#define POINT_H

struct Point{
    int _x, _y;

    Point(int x=0,int y=0):_x(x),_y(y){}
    int getX()const{return _x;}
    int getY()const{return _y;}
};

#endif // POINT_H
