#ifndef COULEUR_H
#define COULEUR_H

struct Couleur
{
    double _r, _g, _b;

    Couleur(double r,double g,double b):_r(r), _g(g), _b(b){}
    
    double getR()const{
        return _r;
    }
    
    double getG()const{
        return _g;
    }
	
    double getB()const{
        return _b;
    }
};

#endif // COULEUR_H
