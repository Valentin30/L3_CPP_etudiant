#include "ligne.h"
#include <iostream>


Ligne::Ligne(const Couleur& couleur,const Point& p0,const Point& p1)
    :FigureGeometrique(couleur),_p0(p0),_p1(p1){}

void Ligne::afficher(const Cairo::RefPtr<Cairo::Context> & context) const{
    context->move_to(getP0().getX(),getP0().getY());
    context->line_to(getP1().getX(),getP1().getY());
    context->set_line_width(2.0);
    context->set_source_rgb(getCouleur().getR(),getCouleur().getG(),getCouleur().getB());
    context->stroke();
}

const Point & Ligne::getP0() const{
    return _p0;
}

const Point & Ligne::getP1() const{
    return _p1;
}
