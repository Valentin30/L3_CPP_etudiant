#ifndef LIGNE_H
#define LIGNE_H

#include "figuregeometrique.h"
#include "point.h"
#include "couleur.h"
#include <cairomm/context.h>

class Ligne : public FigureGeometrique{

private:
    Point _p0, _p1;

public:
    Ligne(const Couleur & couleur, const Point & p0, const Point & p1);
    virtual void afficher(const Cairo::RefPtr<Cairo::Context> & context) const;
    const Point & getP0() const;
    const Point & getP1() const;
};

#endif // LIGNE_H
