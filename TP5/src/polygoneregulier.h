#ifndef POLYGONEREGULIER_H
#define POLYGONEREGULIER_H

#include "figuregeometrique.h"
#include "point.h"
#include <cairomm/context.h>

class PolygoneRegulier : public FigureGeometrique{

private:
    int _nbPoints;
    Point* _points;

public:
    PolygoneRegulier(const Couleur& couleur, const Point& centre, int rayon, int nbCotes);
    virtual void afficher(const Cairo::RefPtr<Cairo::Context> & context) const;
    int getNbPoints() const;
    const Point getPoint(int indice) const;
};

#endif // POLYGONEREGULIER_H
