#include "viewerfigures.h"
#include <iostream>

ViewerFigures::ViewerFigures(int argc, char ** argv) : _kit(argc, argv) {}

void ViewerFigures::run(){
    ZoneDessin zd;

    _window.set_title("Ma fenetre");
    _window.set_default_size(640, 480);
    _window.add(zd);
    _window.show_all();

    Gtk::Main::run(_window);
}
