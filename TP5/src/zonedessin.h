#ifndef ZONEDESSIN_H
#define ZONEDESSIN_H

#include <gtkmm.h>
#include <vector>
#include <iostream>

#include "figuregeometrique.h"
#include "zonedessin.h"
#include "ligne.h"
#include "couleur.h"
#include "point.h"
#include "polygoneregulier.h"


class ZoneDessin : public Gtk::DrawingArea{
private:
    std::vector<FigureGeometrique*> _figures;

public:
    ZoneDessin();
    ~ZoneDessin();
    bool on_expose_event(GdkEventExpose* event);
    bool gererClic(GdkEventButton* event);
};

#endif // ZONEDESSIN_H
