#include "image.h"
#include <fstream>
#include <math.h>

Image::Image(int largeur, int hauteur): _largeur(largeur), _hauteur(hauteur)
{
    _pixels = new int[largeur * hauteur];
}

Image::~Image(){
    delete [] _pixels;
}

int Image::getLargeur() const{
    return _largeur;
}

int Image::getHauteur() const{
    return _hauteur;
}


int & Image::Pixel(int i, int j) const{
    return _pixels[i + j * _largeur];
}

void Image::ecrirePnm(const Image & img, const std::string & nomFichier){
    std::ofstream ofs(nomFichier, std::ofstream::out);

    ofs << "P2" << std::endl;
    ofs << img.getLargeur() << " " << img.getHauteur() << std::endl;
    ofs << 255 << std::endl;
    for (int j = 0; j < img.getHauteur(); j++) {
        for (int i = 0; i < img.getLargeur(); i++) {
            ofs << img.Pixel(i, j) << " ";
        }
        ofs << std::endl;
    }
}

Image Image::bordure(const Image & img, int couleur, int epaisseur){
    Image img2(img.getLargeur()+ epaisseur, img.getHauteur()+ epaisseur);
    remplir(img2);

    for (int j = 0; j < img2.getHauteur(); ++j) {
        for (int i = 0; i < img2.getLargeur(); ++i) {
            if(j <= epaisseur || i <= epaisseur ||
                    j >= img2.getHauteur() - epaisseur || j >= img2.getLargeur() - epaisseur){
                img2.Pixel(i, j) = couleur;
            }
        }
    }
    img2.ecrirePnm(img2, "Image2");
    return img2;
}

void remplir(Image & img){
    for (int j = 0; j < img.getHauteur(); ++j) {
        for (int i = 0; i < img.getLargeur(); ++i) {
            double c = cos(i *3.14 /180.f);
            c = ((c+1) /2) *255;
            img.Pixel(i, j) = c;
        }
    }
}

//int Image::getPixel(int i, int j) const{
//    return _pixels[i + j * _largeur];
//}

//void Image::setPixel(int i, int j, int couleur){
//    _pixels[i + j * _largeur] = couleur;
//}
