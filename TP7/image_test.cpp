#include "image.h"

#include <sstream>
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupImage){};

TEST(GroupImage, Test_image_getLargeur)
{
    Image img(10, 18);
    CHECK_EQUAL(img.getLargeur(), 10);
}

TEST(GroupImage, Test_image_getHauteur)
{
    Image img(10, 18);
    CHECK_EQUAL(img.getHauteur(), 18);
}

TEST(GroupImage, Test_image_getPixels)
{
    Image img(10, 18);
    img.Pixel(1,1) = 1;
    CHECK_EQUAL(img.Pixel(1, 1), 1);
}

