#ifndef IMAGE_H
#define IMAGE_H

#include <string>

class Image{
private:
    int _largeur, _hauteur;
    int * _pixels;

public:
    Image(int largeur, int hauteur);
    ~Image();
    int getLargeur() const;
    int getHauteur() const;
    int & Pixel(int i, int j) const;
    void ecrirePnm(const Image & img, const std::string & nomFichier);
    Image bordure(const Image & img, int couleur, int epaisseur);

    // int getPixel(int i, int j) const;
    // void setPixel(int i, int j, int couleur);
};

void remplir(Image & img);
#endif // IMAGE_H
