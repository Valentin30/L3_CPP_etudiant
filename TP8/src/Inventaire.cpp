#include "Inventaire.hpp"

std::ostream & operator<<(std::ostream & os, const Inventaire & inv){
    for (Bouteille b : inv._bouteilles){
        os << b._nom << ";" << b._date << ";" << b._volume << "\n" ;
    }
    return os;
}
