#include "Controleur.hpp"

#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>

Controleur::Controleur(int argc, char ** argv) {
    _vues.push_back(std::make_unique<VueConsole>(argc, argv, *this));
    _vues.push_back(std::make_unique<VueGraphique>(argc, argv, *this));
}

void Controleur::run() {
    for (auto & v : _vues)
        v->run();
}

std::string Controleur::getTexte() const{
    std::ostringstream oss;
    for(Bouteille b : _inventaire._bouteilles){
        oss << b._nom << ";" << b._date << ";" << b._volume << std::endl;
    }
    std::string s = oss.str();
    return s;
}

void Controleur::chargerInventaire(std::string nom){
    std::ifstream ifs;
    ifs.open(nom, std::ifstream::in);

    Bouteille b;

    while (!ifs.eof()) {
        ifs >> b;
        _inventaire._bouteilles.push_back(b);
    }

    ifs.close();
    for (auto & v : _vues)
        v->actualiser();
}
